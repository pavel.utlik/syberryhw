<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Post;
use Doctrine\ORM\EntityManagerInterface;

class PostRepository
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    /**
     * @var
     */
    private $repository;


    /**
     * PostRepository constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(Post::class);
    }

    /**
     * @param $postId
     * @return object
     */
    public function findOneById($postId)
    {
        return $this->repository->find($postId);
    }

    /**
     * @param $slug
     * @return null|object
     */
    public function findOneByTitle($slug)
    {
        return $this->repository->findOneBy([
            'slug' => $slug
        ]);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }


    /**
     * @param Post $post
     */
    public function save(Post $post)
    {
        $this->entityManager->persist($post);
        $this->entityManager->flush();
    }

    /**
     * @param Post $post
     */
    public function delete(Post $post)
    {
        $this->entityManager->remove($post);
        $this->entityManager->flush();
    }

}
