<?php
namespace AppBundle\Services;

class CalculatorService
{
    public static function sum($a, $b)
    {
        $result = $a + $b;
        return $result;
    }
}