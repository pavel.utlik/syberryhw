<?php

namespace AppBundle\Services;

class ExpressionService
{
    public function __construct(CalculatorService $calculator)
    {
        $this->calculator = $calculator;
    }

    public function calculator($a, $b, $c)
    {
        return $this->calculator->sum($a, $b) + $this->calculator->sum($b, $c);
    }
}