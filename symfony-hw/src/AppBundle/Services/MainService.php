<?php

namespace AppBundle\Services;


abstract class MainService
{

    abstract public function findAll();

    abstract public function findOneById($id);

    abstract public function findOneBySlug($slug);

}
