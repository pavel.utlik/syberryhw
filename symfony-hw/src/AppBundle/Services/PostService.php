<?php

namespace AppBundle\Services;


class PostService extends MainService
{

    private $uniqueValues = array(
        array(
            'id' => '1',
            'title' => 'Dima',
            'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, soluta.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aut corporis culpa debitis fuga harum hic inventore iste laborum necessitatibus nesciunt nihil perferendis provident quae ratione recusandae repellendus repudiandae rerum similique soluta sunt temporibus totam vel velit, voluptas voluptates.'
        ),
        array(
            'id' => '2',
            'title' => 'Pasha',
            'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, soluta.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aut corporis culpa debitis fuga harum hic inventore iste laborum necessitatibus nesciunt nihil perferendis provident quae ratione recusandae repellendus repudiandae rerum similique soluta sunt temporibus totam vel velit, voluptas voluptates.'
        ),
        array(
            'id' => '3',
            'title' => 'Rita',
            'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, soluta.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aut corporis culpa debitis fuga harum hic inventore iste laborum necessitatibus nesciunt nihil perferendis provident quae ratione recusandae repellendus repudiandae rerum similique soluta sunt temporibus totam vel velit, voluptas voluptates.'
        ),
        array(
            'id' => '4',
            'title' => 'Sasha',
            'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, soluta.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aut corporis culpa debitis fuga harum hic inventore iste laborum necessitatibus nesciunt nihil perferendis provident quae ratione recusandae repellendus repudiandae rerum similique soluta sunt temporibus totam vel velit, voluptas voluptates.'
        ),
        array(
            'id' => '5',
            'title' => 'Alisa',
            'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, soluta.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aut corporis culpa debitis fuga harum hic inventore iste laborum necessitatibus nesciunt nihil perferendis provident quae ratione recusandae repellendus repudiandae rerum similique soluta sunt temporibus totam vel velit, voluptas voluptates.'
        ),
        array(
            'id' => '6',
            'title' => 'Slava',
            'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, soluta.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aut corporis culpa debitis fuga harum hic inventore iste laborum necessitatibus nesciunt nihil perferendis provident quae ratione recusandae repellendus repudiandae rerum similique soluta sunt temporibus totam vel velit, voluptas voluptates.'
        ),
        array(
            'id' => '7',
            'title' => 'Valera',
            'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, soluta.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aut corporis culpa debitis fuga harum hic inventore iste laborum necessitatibus nesciunt nihil perferendis provident quae ratione recusandae repellendus repudiandae rerum similique soluta sunt temporibus totam vel velit, voluptas voluptates.'
        ),
        array(
            'id' => '8',
            'title' => 'Svetlana',
            'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, soluta.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aut corporis culpa debitis fuga harum hic inventore iste laborum necessitatibus nesciunt nihil perferendis provident quae ratione recusandae repellendus repudiandae rerum similique soluta sunt temporibus totam vel velit, voluptas voluptates.'
        ),
        array(
            'id' => '9',
            'title' => 'Alex',
            'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, soluta.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aut corporis culpa debitis fuga harum hic inventore iste laborum necessitatibus nesciunt nihil perferendis provident quae ratione recusandae repellendus repudiandae rerum similique soluta sunt temporibus totam vel velit, voluptas voluptates.'
        ),
        array(
            'id' => '10',
            'title' => 'Tanya',
            'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, soluta.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aut corporis culpa debitis fuga harum hic inventore iste laborum necessitatibus nesciunt nihil perferendis provident quae ratione recusandae repellendus repudiandae rerum similique soluta sunt temporibus totam vel velit, voluptas voluptates.'
        ),
        array(
            'id' => '11',
            'title' => 'Vlad',
            'short_description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, soluta.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium aut corporis culpa debitis fuga harum hic inventore iste laborum necessitatibus nesciunt nihil perferendis provident quae ratione recusandae repellendus repudiandae rerum similique soluta sunt temporibus totam vel velit, voluptas voluptates.'
        )
    );

    /**
     * @return array
     */
    public function findAll()
    {

//        $strings = array();
//
//        for ($i = 0; $i < count($this->uniqueValues); $i++)
//        {
//            array_push($strings, $this->uniqueValues[$i]['value']);
//        }

        return $this->uniqueValues;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findOneById($id)
    {

        for ($i = 0; $i < count($this->uniqueValues); $i++)
        {
            if($this->uniqueValues[$i]["id"] == $id)
            {
                return $this->uniqueValues[$i];
            }


        }

        return null;

    }

    public function findOneBySlug($slug)
    {

        for ($i = 0; $i < count($this->uniqueValues); $i++)
        {
            if($slug == $this->uniqueValues[$i]['title'])
            {
                return $this->uniqueValues[$i];
            }

        }

        return null;

    }
}