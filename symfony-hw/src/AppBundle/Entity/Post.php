<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * Class Post
 * @package AppBundle\Entity
 * @ORM\Entity()
 * @ORM\Table(name="post", indexes={@ORM\Index(name="id_idx", columns={"id"})})
 */
class Post
{

    //Every Entity must have an identifier/primary key and I created $id field for primary key
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     * @Assert\Length(min=10, minMessage="Title field is too short. It should have {{ limit }} characters or more.")
     * @Assert\NotBlank()
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="slug", type="string", length=100, nullable=false)
     * @Assert\Regex(pattern="/^[a-z0-9-]*$/", message="Slug field can contain lowercase letters, numbers and dashes")
     */
    private $slug;

    /**
     * @ORM\Column(name="post_at", type="datetime", nullable=true)
     * @Assert\GreaterThan("now", message="Post at field should be greater than {{ compared_value }}")

     */
    private $post_at;

    /**
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var object
     * @ManyToOne(targetEntity="AppBundle\Entity\User")
     * @JoinColumn(name="author_id", referencedColumnName="id")
     */
    private $author;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Post
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set postAt
     *
     * @param \DateTime $postAt
     *
     * @return Post
     */
    public function setPostAt($postAt)
    {
        $this->post_at = $postAt;

        return $this;
    }

    /**
     * Get postAt
     *
     * @return \DateTime
     */
    public function getPostAt()
    {
        return $this->post_at;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Post
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return Post
     */
    public function setAuthor(\AppBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }
}