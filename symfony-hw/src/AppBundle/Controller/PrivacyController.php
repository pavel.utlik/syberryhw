<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class PrivacyController extends Controller
{
    /**
     * @Route("/privacy/policy", name="pravacy-policy")
     */
    public function policyAction(Request $request)
    {
        $this->addFlash('notice',
                        'error!!!');
        $name=$request->get("name",'stop');
        //$request->cookies->get();

       return new Response(

         '<html><body>pravacy-policy'.' '.$name.'<body><html>'
       );
    }
    /**
     * @Route("/privacy/policy/{name}", name="pravacy-policyNamed",requirements={"name"="[a-z A-Z]+"} )
     */
    public function namedAction($name)

    {

        return new Response(
            '<html><body>string'.' '. $name.'<body><html>'
        );
    }
    /**
     * @Route("/privacy/policy/{number}", name="number" , requirements={"number"="\d+"})
     */
    public function numberAction($number)

    {   if ($number == 7)
        throw $this->createNotFoundException();
        return new Response(
            '<html><body>number'.' '. $number.'<body><html>'
        );
    }

    /**
     * @Route("set-session", name="set" )
     */
    public function setSessionAction(SessionInterface $session)
    {
       $session->set("setSession", 42);
       return $this->redirectToRoute("get");

    }
    /**
     * @Route("get-session", name="get" )
     */
    public function getSessionAction(SessionInterface $session)
    {

   $name= $session->get("setSession");

        return new Response(
            '<html><body>string'.' '. $name.'<body><html>'
        );
    }


    /**
     * @Route("clients/clients/", name="clients" )

     */
    public function getClients()
    {
      $clients = [
            ['name'=>'pavel','surname'=>'utlik', 'phone'=>'+375298610836'  ],
            ['name'=>'dmitriy', 'surname'=>'gorbik', 'phone'=>'+37529224352'  ],
            ['name'=>'alex', 'surname'=>'metelskiy','phone'=>'+37529223356'  ],
            ['name'=>'Rita', 'surname'=>'troshko','phone'=>'+37529226789'  ],
            ['name'=>'Alisa', 'surname'=>'utlik','phone'=>'+375298600836'  ],
            ];
        return $this->render('clients.html.twig', [
            'clients' => $clients]);
    }


}

