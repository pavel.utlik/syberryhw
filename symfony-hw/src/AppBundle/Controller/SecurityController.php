<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{


    /**
     * @Route("/login", name="login")
     * @param Request $request
     * @param AuthenticationUtils $authenticationUtils
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request, AuthenticationUtils $authenticationUtils)
    {

//        var_dump($request);
//        exit();

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUser = $authenticationUtils->getLastUsername();

        return $this->render('security/security.html.twig', [
            'last_username' => $lastUser,
            'error' => $error
        ]);

    }


    /**
     * @Route("/logout", name="logout")
     * @throws \RuntimeException
     */
    public function logoutAction()
    {

        throw new \RuntimeException('logout');

    }


}
