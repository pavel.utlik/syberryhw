<?php

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

abstract class MainController extends Controller
{

    abstract protected function indexAction(Request $request, $page = null, $pageId);

    abstract protected function showAction(Request $request, $id);

    abstract protected function editAction(Request $request, $id);

    abstract protected function deleteAction(Request $request, $id);

}
