<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Form\PostCreate;
use AppBundle\Repository\PostRepository;
use AppBundle\Services\PostService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class PostController extends MainController
{

    /**
     * @var PostService
     */
    private $postService;

    /**
     * @var PostRepository
     */
    private $postRepository;

    /**
     * PostController constructor.
     * @param PostService $postService
     * @param PostRepository $postRepository
     */
    public function __construct(PostService $postService, PostRepository $postRepository)
    {
        $this->postService = $postService;
        $this->postRepository = $postRepository;
    }


    /**
     * @Route("/admin/user/{page}/{pageId}/{_locale}", methods={"GET"}, name="index-action-post",
     * requirements={"page"="[a-zA-Z]+$", "pageId"="\d+"}, defaults={"_locale"="en"})
     * @param Request $request
     * @param null $page
     * @param int $pageId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page = null, $pageId)
    {
        $response = $this->postRepository->findAll();

        $arr = $this->getSerializer()->normalize($response);

        return $this->render('default/post/index.html.twig', [
            'array' => $response
        ]);

    }

    /**
     * @Route("/admin/user/{id}", methods={"GET"}, name="show-action-post",
     * requirements={"id"="\d+"}, defaults={"_locale"="en"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, $id)
    {

        $response = $this->postRepository->findOneById($id);

        $arr = $this->getSerializer()->normalize($response);

        if($response == null){
            return new Response("This page not found!", 404);
        }

        return $this->render('default/post/show.html.twig', [
            'string' => $arr
        ]);

    }

    /**
     * @Route("/admin/user/new", methods={"GET", "POST"}, name="edit-action-post-first")
     * @Route("/admin/user/{id}/edit/{_locale}", methods={"GET", "POST"}, name="edit-action-post-second",
     * requirements={"id"="\d+"}, defaults={"_locale"="en"})
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function editAction(Request $request, $id = 0)
    {

        define('second_route', 'edit-action-post-second');

        $routeName = $request->get('_route');

        if($routeName != second_route) {
            // Some logic for /admin/user/new

            $post = new Post();

            $form = $this->createForm(PostCreate::class, $post);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $post = $form->getData();

                $this->postRepository->save($post);

                return $this->redirectToRoute('index-action-post', [
                    'page' => 'something',
                    'pageId' => 1
                ]);

            }

            return $this->render('default/post/edit.html.twig', [
                'form' => $form->createView()
            ]);

        }



        $post = $this->postRepository->findOneById($id);

        $form = $this->createForm(PostCreate::class, $post);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $post = $form->getData();

            $this->postRepository->save($post);

            return $this->redirectToRoute('index-action-post', [
                'page' => 'some',
                'pageId' => 2
            ]);

        }

        return $this->render('default/post/edit.html.twig', [
            'form' => $form->createView()
        ]);


    }

    /**
     * @Route("/admin/user/{id}/delete/{_locale}", methods={"DELETE"}, name="delete-action-post",
     * requirements={"id"="\d+"}, defaults={"_locale"="en"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, $id)
    {

        $response = $this->postService->findAll();

        if($response == null){
            return new Response("This page not found!", 404);
        }

        $message = "Message with id = " . $id . " has been successful deleted";

        return $this->render('default/post/index.html.twig', [
            'message' => $message,
            'array' => $response
        ]);

    }

    /**
     * @Route("/post/{pageId}/{_locale}", methods={"GET"}, name="list-action",
     * requirements={"pageId"="\d+"}, defaults={"_locale"="en"})
     * @param Request $request
     * @param $pageId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction(Request $request, $pageId = 1)
    {

        $response = $this->postRepository->findAll();

        $arr = $this->getSerializer()->normalize($response);

        return $this->render('default/post/list.html.twig', [
            'array' => $arr
        ]);

    }

    /**
     * @Route("/post/{slug}/{_locale}", methods={"GET"}, name="view-action",
     * requirements={"slug"="[a-zA-Z]+$"}, defaults={"_locale"="en"})
     * @param Request $request
     * @param $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Request $request, $slug)
    {

        $response = $this->postRepository->findOneByTitle($slug);

        $arr = $this->getSerializer()->normalize($response);

        if($response == null){
            return new Response("This page not found!", 404);
        }

        return $this->render('default/post/view.html.twig', [
            'string' => $arr
        ]);

    }

    /**
     * @Route("/success", methods={"GET"}, name="success-action")
     */
    public function successAction()
    {
        return $this->render('default/post/success.html.twig');
    }

    /**
     * @return Serializer
     */
    private function getSerializer()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        return $serializer = new Serializer($normalizers, $encoders);
    }

}
