<?php

namespace AppBundle\Controller;

use AppBundle\Services\WorkingDaysService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WorkingDaysController extends Controller
{

    private $workingDaysService;

    /**
     * WorkingDaysController constructor.
     * @param $workingDaysService
     */
    public function __construct(WorkingDaysService $workingDaysService)
    {
        $this->workingDaysService = $workingDaysService;
    }


    /**
     * @Route("/working_days/{firstDate}/{secondDate}", methods={"GET", "POST"}, name="working-days")
     * @param Request $request
     * @param $firstDate
     * @param $secondDate
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function getWorkingDaysAction(Request $request, $firstDate, $secondDate){

        $json = $request->getContent();

        // This array contains random date included in the range of values $firstDate and $secondDate
        $arrayDaysOff = json_decode($json, true);
        $arrayDaysOff = $this->translateArrayValuesToDate($arrayDaysOff);

        $countWorkingDays = $this->workingDaysService->getWorkingDays($firstDate, $secondDate, $arrayDaysOff);

        return $this->render('default/index.html.twig', [
           'count' => $countWorkingDays
        ]);

    }

    private function translateArrayValuesToDate(array $array){

        for ($i = 0; $i < count($array); $i++){
            $array[$i] = strtotime($array[$i]);
        }

        return $array;
    }


    /**
     * @Route("/working_days_new/{firstDate}/{secondDate}/{daysOffString}", name="working-days-new")
     * @param $firstDate
     * @param $secondDate
     * @param $daysOffString
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getWorkingDaysNewAction($firstDate, $secondDate, $daysOffString){

        //Translate string to array
        $daysOffString = explode(',', $daysOffString);
        $daysOffString = $this->translateArrayValuesToDate($daysOffString);

        $countWorkingDays = $this->workingDaysService->getWorkingDays($firstDate, $secondDate, $daysOffString);

        return $this->render('default/index.html.twig', [
           'count' => $countWorkingDays
        ]);
    }

}