<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserCreate;
use AppBundle\Services\UserService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Repository\UserRepository;


use AppBundle\Entity\User;

class UserController extends MainController
{

    /**
     * @var UserService
     */
    private $userService;


    /**
     * @var UserRepository
     */
    private $userRepository;


    public function __construct(UserService $userService, UserRepository $userRepository)
    {
        $this->userService = $userService;
        $this->userRepository = $userRepository;
    }




    /**
     * @Route("/admin/{page}/{pageId}/{_locale}", methods={"GET"},
     *  name="index-action", requirements={"page"="[a-zA-Z]+$", "pageId"="\d+"}, defaults={"_locale"="en"})
     * @param Request $request
     * @param $page
     * @param int $pageId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page = null, $pageId)
    {

        $response = $this->userRepository->findAll();

        return $this->render('default/user/index.html.twig', [
            'array' => $response
        ]);

    }


    /**
     * @Route("/admin/{id}", methods={"GET"}, name="show-action",
     * requirements={"id"="\d+"}, defaults={"_locale"="en"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, $id)
    {

        $response = $this->userRepository->findOneById($id);

        if($response == null){
            return new Response("This page not found!", 404);
        }

        return $this->render('default/user/show.html.twig', [
            'string' => $response
        ]);

    }

    /**
     * @Route("/admin/new/{_locale}", methods={"GET", "POST"}, name="edit-action-first")
     * @Route("/admin/{id}/edit/{_locale}", methods={"GET", "POST"}, name="edit-action-second",
     * requirements={"id"="\d+"}, defaults={"_locale"="en"})
     * @param Request $request
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id = 0)
    {
        define('second_route', 'edit-action-second');

        $routeName = $request->get('_route');

        if($routeName != second_route){



            $user = new User();

            $form = $this->createForm(UserCreate::class, $user);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $user = $form->getData();

                $this->userRepository->save($user);

                return $this->redirectToRoute('index-action', [
                    'page' => 'something',
                    'pageId' => 1
                ]);

            }

            return $this->render('default/user/edit.html.twig', [
                'form' => $form->createView()
            ]);

        }

        //admin/{id}/edit

        $user = $this->userRepository->findOneById($id);

        $form = $this->createForm(UserCreate::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $user = $form->getData();

            $this->userRepository->save($user);

            return $this->redirectToRoute('index-action', [
                'page' => 'some',
                'pageId' => 1
            ]);

        }

        return $this->render('default/user/edit.html.twig', [
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/admin/{id}/delete/{_locale}", methods={"DELETE"}, name="delete-action",
     * requirements={"id"="\d+"}, defaults={"_locale"="en"})
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request, $id)
    {

        $response = $this->userService->findAll();

        if($response == null){
            return new Response("This page not found!", 404);
        }

        $message = "Message with id = " . $id . " has been successful deleted";

        return $this->render('default/user/index.html.twig', [
            'message' =>  $message,
            'array' => $response
        ]);

    }
}

