<?php

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserCreate extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Email', EmailType::class, [
                'required' => false
            ])
            ->add('Username', TextType::class)
            ->add('First_name', TextType::class)
            ->add('Last_name', TextType::class)
            ->add('Password', PasswordType::class, [
                'required' => true
            ])

            ->add('Save', SubmitType::class);
    }


}
