<?php

function autoloader($class)
{

    $class = str_replace("\\", '/', $class);
    $file = __DIR__ . "/{$class}.php";
    if(file_exists($file)){
        require_once $file;
    }

}

spl_autoload_register('autoloader');

if(isset($argv) && count($argv) > 1){

    $partsArguments = explode('=', $argv[1]);
    $user_id = (int)$partsArguments[1];

    $sub = new Academy\Controller\SubscriptionController();
    $sub->cancelSubscriptionForUser($user_id);

} else {

    echo "There is no user_id argument...";

//    $sub = new Academy\Controller\SubscriptionController();
//    $sub->cancelSubscriptionForUser(1);

}



