<?php

namespace Academy\Data\Subscription;

use Academy\Data\User;

class SubscriptionRepository
{
    public function getActiveSubscription(User $user)
    {
        // condition in order to emulate error
        if ($user->getId() % 10 != 0) {
            return new Subscription($user->getId());
        } else {
            return null;
        }
    }
}