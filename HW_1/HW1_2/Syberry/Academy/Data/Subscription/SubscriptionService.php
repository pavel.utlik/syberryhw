<?php

namespace Academy\Data\Subscription;
use Academy\Infrastructure\SubscriptionGatewayApi;
use Academy\ExceptionClasses\FailedCanelSubscriptionKey;
use Academy\ExceptionClasses\NotFoundActiveSubscriptionKey;


class SubscriptionService
{

    /**
     * @var SubscriptionRepository
     */
    private $subscriptionRepository;

    /**
     * @var SubscriptionGatewayApi
     */
    private $subscriptionGatewayApi;

    /**
     * SubscriptionService constructor.
     */
    public function __construct()
    {
        $this->subscriptionRepository = new SubscriptionRepository();
        $this->subscriptionGatewayApi = new SubscriptionGatewayApi();
    }


    /**
     * @param $user
     * @return array
     * @throws FailedCanelSubscriptionKey
     * @throws NotFoundActiveSubscriptionKey
     */
    public function cancelSubscription($user)
    {

        $activeSubscription = $this->subscriptionRepository->getActiveSubscription($user);
        if (!empty($activeSubscription)) {
            try {
                $this->subscriptionGatewayApi->cancel($activeSubscription->getGatewayId());
            } catch (\Exception $exception) {
                throw new FailedCanelSubscriptionKey();
            }
        } else {
            throw new NotFoundActiveSubscriptionKey();
        }

    }
}