<?php

namespace Academy\Controller;
use Academy\Data\Subscription\SubscriptionService;
use Academy\Data\User;
use Academy\ExceptionClasses\FailedCanelSubscriptionKey;
use Academy\ExceptionClasses\NotFoundActiveSubscriptionKey;
use Academy\Infrastructure\Http\Response;

class SubscriptionController
{
    private $subscriptionService;

    /**
     * SubscriptionController constructor.
     */
    public function __construct()
    {
        $this->subscriptionService = new SubscriptionService();
    }


    public function cancelSubscriptionForUser($userId)
    {

        try{

            $user = new User($userId);
            $this->subscriptionService->cancelSubscription($user);

            echo "Code status: 200\nSubscription has been canel\n";
            return new Response(200);

        } catch (FailedCanelSubscriptionKey $e) {
            echo "Code status: " . FailedCanelSubscriptionKey::CODE . "\nThe subscription wasn't cancelled due to technical issue\n";
            return new Response(FailedCanelSubscriptionKey::CODE, [
                'message' => 'The subscription wasn\'t cancelled due to technical issue'
            ]);
        } catch (NotFoundActiveSubscriptionKey $e) {
            echo "Code status: " . NotFoundActiveSubscriptionKey::CODE . "\nSubscription is not found for user\n";
            return new Response(NotFoundActiveSubscriptionKey::CODE, [
                'message' => 'Subscription is not found for user'
            ]);
        }


    }
}
