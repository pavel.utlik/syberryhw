<?php

namespace Academy\Infrastructure;

class SubscriptionGatewayApi
{

    /**
     * @param $id
     * @throws Exception
     */
    public function cancel($id)
    {
        // emulates error
        if ($id % 101 == 0) {
            throw new Exception();
        }
    }
}