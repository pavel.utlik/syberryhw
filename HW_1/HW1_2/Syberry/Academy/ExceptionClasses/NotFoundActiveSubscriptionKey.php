<?php

namespace Academy\ExceptionClasses;

class NotFoundActiveSubscriptionKey extends \Exception
{

    const NOT_FOUND_ACTIVE_SUBSCRIPTION_KEY = 2;

    const CODE = 404;

}